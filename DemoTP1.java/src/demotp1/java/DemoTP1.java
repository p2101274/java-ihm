/**
 * Question 1:
 * L'API swing se trouve dans le package javax
 *
 * Question 5:
 * Combo box
 * Selon la Javadoc : "Un composant qui combine un bouton ou un champ modifiable et une liste déroulante. L'utilisateur peut sélectionner une valeur dans la liste déroulante, qui apparaît à la demande de l'utilisateur. Si vous rendez la liste déroulante modifiable, celle-ci comprend un champ modifiable dans lequel l'utilisateur peut saisir une valeur."
 * Il s'agit d'un bouton permettant d'accéder à plusieurs options, chaque option peut être renseigné dans une list ou ajouter avec la méthode de l'objet "add"
 *
 * Selon la Javadoc : "Une JTextArea est une zone multi-lignes qui affiche du texte brut. Il s'agit d'un composant léger qui offre une compatibilité source avec la classe java.awt.TextArea lorsqu'il peut raisonnablement le faire. Vous trouverez des informations et des exemples d'utilisation de tous les composants texte dans la section Using Text Components du Java Tutorial. Ce composant possède des capacités qui n'existent pas dans la classe java.awt.TextArea. Il convient de consulter la superclasse pour obtenir des capacités supplémentaires. D'autres classes de texte multiligne offrant davantage de possibilités sont JTextPane et JEditorPane.
 * Il s'agit d'un panneau avec une zonne de texte. Ce panneau n'est pas déroulant par défault. Le retour à la ligne n'est pas activé par default. Plusieurs paramettre peuvent être indiqué, pour sa taille etc
 *
 * Question 9 :
 * Oui il y a des différences, le choix de l'année, le label des préférences sont au centre.
 */
package demotp1.java;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;

/**
 * Cette classe permet de créer un objet qui génèrera une fenettre contenant une interface.
 * @author Jérémie Vernay
 */
public class DemoTP1 extends JFrame implements WindowListener{

//ATTRIBUTS
    /**
     * Correspond au different texte afficher sur la fenettre.
     */
    private JLabel lbl_etatCivilImage, lbl_etatCivil, lbl_commentaire, lbl_nom, lbl_prenom, lbl_annee, lbl_preferences, lbl_sem;

    /**
     * Correspond à une combo box, {@link  #cbb_etatCivil} correspond au sexe de la personne
     */
    private JComboBox cbb_etatCivil;

    /**
     * Correspond à deux zones de texte permetant à l'utilisateur de le renseigné le nom dans {@link #tf_nom} et le prénom dans {@link  #tf_prenom}
     */
    private JTextField tf_nom, tf_prenom, tf_autre;

    /**
     * Correspond à deux bouton dit radius permetant de choisir l'anné de la personne
     */
    private JRadioButton rb_annee1, rb_annee2, rb_sem1, rb_sem2, rb_sem3, rb_sem4;

    /**
     * Correspond à 4 checkbox box permettant d'indiqué son niveau dans ses club
     */
    private JCheckBox ckb_robotoique, ckb_echec, ckb_logicielLibre, ckb_jeudego, ckb_autre;

    /**
     * Correspond à 4 combos box permettant de choisir ses club
     */
    private JComboBox cbb_robotoique, cbb_echec, cbb_logicielLibre, cbb_jeudego, cbb_autre;

    /**
     * Correspond à une zonne de texte pour ajouter un commentaire
     */
    private JTextArea ta_commentaire;

    /**
     * Correspond au bouton pour savoir quoi faire avec les info
     */
    private JButton bt_modifier, bt_supprimer, bt_ajouter;

    /**
     * Correspond à un pannel avec une barre de déffilement.
     * Il sert à défiler dans le texte du commentaire
     */
    private JScrollPane ta_scroll;

    private ButtonGroup radiobtnA, radiobtnSem1, radiobtnSem2;

    /**
     * Correspond au différent pannel utiliser affin de ranger les différent éléments
     */
    private JPanel pnl_etatcivil,pnl_info, pnl_nom,pnl_anne, pnl_semA1, pnl_semA2, pnl_txt,pnl_commentaire,pnl_loisir,pnl_autre, pnl_validation;


//CONSTRUCTORS
    /**
     * Crée un objet {@link DemoTP1}
     */
    public DemoTP1() {
        initComponents(); //Ajoute tout les composant à la fenetre
        this.pack();
        this.setVisible(true); //rend la fenetre visible
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //arrete le programme quand la fenettre est fermer
        this.setResizable(false); //empèche le redimensionnement de la fenetre
        this.addWindowListener(this);
    }

    /**
     * Crée un objet {@link DemoTP1}
     * @param nomFenetre Choisi le nom de la fenetre
     */
    public DemoTP1(String nomFenetre) {
        initComponents();
        this.pack();
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setTitle(nomFenetre); //applique un nom à la fenetre
        this.addWindowListener(this);
    }
//METHODES PRIVE
    /**
     * Initialise et dispose tout les éléments dans la fenetre
     */
    private void initComponents() {

        //défini l'image pour le label image
        lbl_etatCivilImage = new JLabel(new ImageIcon("src/gif/fille.jpg")); //Je ne sais pas pourquoi mais sur ma tour le chemin relatif ne fonctionne pas
        //defini le texte pour les autre label
        lbl_etatCivil = new JLabel("Etat civil : ");
        lbl_commentaire = new JLabel("Commentaire");
        lbl_nom = new JLabel("Nom : ");
        lbl_prenom =  new JLabel("Prénom : ");
        lbl_annee = new JLabel("Année: ");
        lbl_preferences = new JLabel("Préférences:");
        lbl_sem = new JLabel(" ---> Semestre: ");
        //defini la longeur des champs de texte
        tf_nom = new JTextField(12);
        tf_prenom = new JTextField(12);
        tf_autre = new JTextField(9);

        //défini la combo box
        cbb_etatCivil = new JComboBox();
        //ajout des champs de la combo box
        cbb_etatCivil.addItem("Mme");
        cbb_etatCivil.addItem("M");
        cbb_etatCivil.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                if (cbb_etatCivil.getSelectedItem() == "M")
                    lbl_etatCivilImage.setIcon(new ImageIcon("src/gif/garcon.jpg"));
                else
                    lbl_etatCivilImage.setIcon(new ImageIcon("src/gif/fille.jpg"));
            }
        });

        //défini les bouton radio
        rb_annee1 = new JRadioButton("1A");
        rb_annee1.addActionListener(new Alistener());
        rb_annee2 = new JRadioButton("2A");
        rb_annee2.addActionListener(new Alistener());
        rb_sem1 = new JRadioButton("s1");
        rb_sem2 = new JRadioButton("s2");
        rb_sem3 = new JRadioButton("s3");
        rb_sem4 = new JRadioButton("s4");

        //Créé un groupe de bouton pour les bouton radio
        radiobtnA = new ButtonGroup();
        //ajoute les bouton radio au groupe
        radiobtnA.add(rb_annee1);
        radiobtnA.add(rb_annee2);

        radiobtnSem1 = new ButtonGroup();
        //ajoute les bouton radio au groupe
        radiobtnSem1.add(rb_sem1);
        radiobtnSem1.add(rb_sem2);

        radiobtnSem2 = new ButtonGroup();
        //ajoute les bouton radio au groupe
        radiobtnSem2.add(rb_sem3);
        radiobtnSem2.add(rb_sem4);

        //défini les check box
        ckb_robotoique = new JCheckBox("Robotique");
        ckb_echec = new JCheckBox("Jeu de Go");
        ckb_logicielLibre = new JCheckBox("Jeu Echec");
        ckb_jeudego = new JCheckBox("Logiciels libres");
        ckb_autre = new JCheckBox("Autre");
        //défini les combo box
        cbb_robotoique = new JComboBox();
        cbb_jeudego = new JComboBox();
        cbb_echec = new JComboBox();
        cbb_logicielLibre = new JComboBox();
        cbb_autre = new JComboBox();

        //réduit la taille des combot box
        cbb_robotoique.setBorder(new EmptyBorder(5,5,5,5));
        cbb_jeudego.setBorder(new EmptyBorder(5,5,5,5));
        cbb_echec.setBorder(new EmptyBorder(5,5,5,5));
        cbb_logicielLibre.setBorder(new EmptyBorder(5,5,5,5));
        cbb_autre.setBorder(new EmptyBorder(5,5,5,5));

        //ajoute les champs au combo box
        cbb_robotoique.addItem("débutant");
        cbb_robotoique.addItem("moyen");
        cbb_robotoique.addItem("expert");

        cbb_jeudego.addItem("débutant");
        cbb_jeudego.addItem("moyen");
        cbb_jeudego.addItem("expert");

        cbb_echec.addItem("débutant");
        cbb_echec.addItem("moyen");
        cbb_echec.addItem("expert");

        cbb_logicielLibre.addItem("débutant");
        cbb_logicielLibre.addItem("moyen");
        cbb_logicielLibre.addItem("expert");

        cbb_autre.addItem("débutant");
        cbb_autre.addItem("moyen");
        cbb_autre.addItem("expert");


        //défini la zone de texte
        ta_commentaire = new JTextArea("Commentaire:",5,35);
        //active le retour à la ligne
        ta_commentaire.setLineWrap(true);
        //ne fait le retour à la ligne que pour les mots
        ta_commentaire.setWrapStyleWord(true);

        //défini les boutons
        bt_supprimer = new JButton("Supprimer");
        bt_supprimer.addMouseListener(new Mlistener());
        bt_modifier = new JButton("Modifier");
        bt_modifier.addMouseListener(new Mlistener());
        bt_ajouter = new JButton("Ajouter");
        bt_ajouter.addMouseListener(new Mlistener());

        //défini le paneau principal
        JPanel panneau = (JPanel)this.getContentPane();
        //lui défini comme layout un gride layout de 5 par 1
        panneau.setLayout(new GridLayout(5,1));

        //défini le panel etat civil et l'ajoute au principal
        pnl_etatcivil = new JPanel();
        pnl_etatcivil.add(lbl_etatCivilImage);
        pnl_etatcivil.add(lbl_etatCivil);
        pnl_etatcivil.add(cbb_etatCivil);
        panneau.add(pnl_etatcivil);

        //défini le panel information civil et l'ajoute au principal celui ci est une gride composer de plusieur panneau
        pnl_info = new JPanel();
        pnl_info.setLayout(new GridLayout(3,1));

        pnl_nom = new JPanel();
        pnl_nom.add(lbl_nom);
        pnl_nom.add(tf_nom);
        pnl_nom.add(lbl_prenom);
        pnl_nom.add(tf_prenom);
        pnl_info.add(pnl_nom);

        //les éléments du paneau anné serons sur la gauche
        pnl_anne = new JPanel(new FlowLayout(FlowLayout.LEFT));
        pnl_anne.add(lbl_annee);
        pnl_anne.add(rb_annee1);
        pnl_anne.add(rb_annee2);
        lbl_sem.setVisible(false);
        pnl_anne.add(lbl_sem);

        pnl_semA1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        pnl_semA1.add(rb_sem1);
        pnl_semA1.add(rb_sem2);
        pnl_semA1.setVisible(false);

        pnl_semA2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        pnl_semA2.add(rb_sem3);
        pnl_semA2.add(rb_sem4);
        pnl_semA2.setVisible(false);

        pnl_anne.add(pnl_semA1);
        pnl_anne.add(pnl_semA2);
        pnl_info.add(pnl_anne);



        //les éléments du paneau texte serons sur la gauche
        pnl_txt = new JPanel(new FlowLayout(FlowLayout.LEFT));
        pnl_txt.add(lbl_preferences);
        pnl_info.add(pnl_txt);

        panneau.add(pnl_info);


        //défini le panel loisir et l'ajoute au principal
        pnl_loisir = new JPanel();
        pnl_loisir.setLayout(new GridLayout(3,1));

        JPanel pnl_ckb = new JPanel();
        pnl_ckb.setLayout(new GridLayout(1,5));
        pnl_ckb.add(ckb_robotoique);
        pnl_ckb.add(ckb_echec);
        pnl_ckb.add(ckb_logicielLibre);
        pnl_ckb.add(ckb_jeudego);
        pnl_ckb.add(ckb_autre);
        pnl_loisir.add(pnl_ckb);

        JPanel pnl_txt = new JPanel();
        pnl_txt.setLayout(new FlowLayout(FlowLayout.RIGHT));
        pnl_txt.add(tf_autre);
        pnl_loisir.add(pnl_txt);

        JPanel pnl_cbb = new JPanel();
        pnl_cbb.setLayout(new GridLayout(1,5));
        pnl_cbb.add(cbb_robotoique);
        pnl_cbb.add(cbb_echec);
        pnl_cbb.add(cbb_logicielLibre);
        pnl_cbb.add(cbb_jeudego);
        pnl_cbb.add(cbb_autre);
        pnl_loisir.add(pnl_cbb);
        panneau.add(pnl_loisir);


        //défini le panel commentaire et l'ajoute au principal
        pnl_commentaire = new JPanel();
        pnl_commentaire.add(lbl_commentaire);
        ta_scroll = new JScrollPane(ta_commentaire);
        //block la possibilité de d'apparition d'une bare de défilement horizontal
        ta_scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        pnl_commentaire.add(ta_scroll);

        panneau.add(pnl_commentaire);

        //défini le panel validation et l'ajoute au principal
        pnl_validation = new JPanel();
        pnl_validation.add(bt_supprimer);
        pnl_validation.add(bt_modifier);
        pnl_validation.add(bt_ajouter);

        panneau.add(pnl_validation);
    }

//METHODES PUBLIC
    /**
     * Créer une fenetre pour le programme
     */
    public static void main(String[] args){
        DemoTP1 maFenetre = new DemoTP1("Un nom de fenetre");
    }


    @Override
    public void windowOpened(WindowEvent windowEvent) {

        this.setTitle(this.getTitle() + " De Lyon");
        System.out.println("test");

    }

    @Override
    public void windowClosing(WindowEvent windowEvent) {
        
        System.out.println("fermture fenetre");
        int resultat = JOptionPane.showConfirmDialog(this, "Voulez vous vraiment fermer la fenetre ?",
                "Quittez ?", JOptionPane.YES_NO_OPTION);

        if (resultat == JOptionPane.YES_OPTION) {
            this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        } else {
            this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        }

    }

    @Override
    public void windowClosed(WindowEvent windowEvent) {
        System.out.println("fenetre ferme");

    }

    @Override
    public void windowIconified(WindowEvent windowEvent) {
        System.out.println("minimise la fenetre");
    }

    @Override
    public void windowDeiconified(WindowEvent windowEvent) {
        System.out.println("maximise la fenetre");
    }

    @Override
    public void windowActivated(WindowEvent windowEvent) {
        System.out.println("fenetre sélectioné");
    }

    @Override
    public void windowDeactivated(WindowEvent windowEvent) {
        System.out.println("fenetre désélectioné");
    }


    public class Mlistener implements MouseListener{

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            if (mouseEvent.getSource().equals(bt_modifier)) {
                System.out.println("bouton modifier cliqué");
                JOptionPane.showMessageDialog((Component) mouseEvent.getComponent().getParent().getParent(), "Modifier directement les champs dans la fenêtre", "titre", JOptionPane.INFORMATION_MESSAGE);
            }
            if (mouseEvent.getSource().equals(bt_supprimer)) {
                int resultat = JOptionPane.showConfirmDialog((Component) mouseEvent.getComponent().getParent().getParent(), "voulez vous vraiment supprimer l'étudiant ?", "Confirmation suppression",JOptionPane.YES_NO_OPTION);
                System.out.println("bouton supprimer cliqué");
                if(resultat == JOptionPane.YES_OPTION) {
                    cbb_etatCivil.setSelectedIndex(0);
                    pnl_semA1.setVisible(false);
                    pnl_semA2.setVisible(false);
                    lbl_sem.setVisible(false);
                    ckb_autre.setSelected(false);
                    ckb_echec.setSelected(false);
                    ckb_jeudego.setSelected(false);
                    ckb_robotoique.setSelected(false);
                    ckb_logicielLibre.setSelected(false);
                    cbb_autre.setSelectedIndex(0);
                    cbb_echec.setSelectedIndex(0);
                    cbb_jeudego.setSelectedIndex(0);
                    cbb_robotoique.setSelectedIndex(0);
                    cbb_logicielLibre.setSelectedIndex(0);
                    ta_commentaire.setText("");
                    tf_autre.setText("");
                    tf_nom.setText("");
                    tf_prenom.setText("");
                    radiobtnA.clearSelection();
                    radiobtnSem1.clearSelection();
                    radiobtnSem2.clearSelection();
                }
            }
            if (mouseEvent.getSource().equals(bt_ajouter)) {
                System.out.println("bouton ajouter cliqué");
                if (
                        tf_nom.getText().isBlank() ||
                        tf_prenom.getText().isBlank() ||
                        !(rb_annee1.isSelected() || rb_annee2.isSelected()) ||
                        !(rb_sem1.isSelected() || rb_sem2.isSelected() || rb_sem3.isSelected() || rb_sem4.isSelected()) ||
                        (ckb_autre.isSelected() && tf_autre.getText().isBlank())) {

                    if (tf_nom.getText().isBlank()) {
                        tf_nom.setBackground(Color.red);
                    } else {
                        tf_nom.setBackground(Color.white);
                    }

                    if (tf_prenom.getText().isBlank()) {
                        tf_prenom.setBackground(Color.red);
                    } else {
                        tf_prenom.setBackground(Color.white);
                    }

                    if (!(rb_annee1.isSelected() || rb_annee2.isSelected())) {
                        rb_annee1.setBackground(Color.red);
                        rb_annee2.setBackground(Color.red);
                    }
                    else {
                        rb_annee1.setBackground(null);
                        rb_annee2.setBackground(null);
                        if (!(rb_sem1.isSelected() || rb_sem2.isSelected() || rb_sem3.isSelected() || rb_sem4.isSelected())) {
                            rb_sem1.setBackground(Color.red);
                            rb_sem2.setBackground(Color.red);
                            rb_sem3.setBackground(Color.red);
                            rb_sem4.setBackground(Color.red);
                        }
                        else {
                            rb_sem1.setBackground(null);
                            rb_sem2.setBackground(null);
                            rb_sem3.setBackground(null);
                            rb_sem4.setBackground(null);
                        }
                    }

                    if (ckb_autre.isSelected() && tf_autre.getText().isBlank()) {
                        tf_autre.setBackground(Color.red);
                    } else {
                        tf_autre.setBackground(Color.white);
                    }


                } else {
                    tf_nom.setBackground(Color.white);
                    tf_prenom.setBackground(Color.white);
                    rb_annee1.setBackground(null);
                    rb_annee2.setBackground(null);
                    rb_sem1.setBackground(null);
                    rb_sem2.setBackground(null);
                    rb_sem3.setBackground(null);
                    rb_sem4.setBackground(null);
                    tf_autre.setBackground(Color.white);
                    String anne_semestre = "";
                    String club = "";
                    String commentaire = "";
                    if (rb_annee1.isSelected()) {
                        anne_semestre = "1ère anné en semestre ";
                        if (rb_sem1.isSelected()) {
                            anne_semestre += "S1 ";
                        } else {
                            anne_semestre += "S2 ";
                        }
                    } else {
                        anne_semestre = "2ème anné en semestre ";
                        if (rb_sem1.isSelected()) {
                            anne_semestre += "S3 ";
                        } else {
                            anne_semestre += "S4 ";
                        }
                    }
                    if (ckb_robotoique.isSelected() || ckb_jeudego.isSelected() || ckb_echec.isSelected() || ckb_logicielLibre.isSelected() || ckb_autre.isSelected()) {
                        club += "\net souhaite participer au(x) club(s) : ";
                        if (ckb_robotoique.isSelected()) {
                            club += "robotoique ";
                        }
                        if (ckb_jeudego.isSelected()) {
                            club += "jeu de go ";
                        }
                        if (ckb_echec.isSelected()) {
                            club += "echec ";
                        }
                        if (ckb_logicielLibre.isSelected()) {
                            club += "logiciel libre ";
                        }
                        if (ckb_autre.isSelected()) {
                            club += tf_autre.getText() + " ";
                        }
                    }
                    System.out.println();
                    if(!ta_commentaire.getText().equals("Commentaire:")) {
                        commentaire = "\nAutres informations : "+ ta_commentaire.getText();
                    }


                    String msg = cbb_etatCivil.getSelectedItem() + " " + tf_prenom.getText() + " " + tf_nom.getText() + "\nen " + anne_semestre + club + commentaire;
                    JOptionPane.showMessageDialog(null, msg, "titre", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            if (mouseEvent.getSource().equals(bt_modifier)) {
                System.out.println("bouton modifier presser");
            }
            if (mouseEvent.getSource().equals(bt_supprimer)) {
                System.out.println("bouton supprimer presser");
            }
            if (mouseEvent.getSource().equals(bt_ajouter)) {
                System.out.println("bouton ajouter presser");
            }

        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {
            if (mouseEvent.getSource().equals(bt_modifier)) {
                System.out.println("bouton modifier relacher");
            }
            if (mouseEvent.getSource().equals(bt_supprimer)) {
                System.out.println("bouton supprimer relacher");
            }
            if (mouseEvent.getSource().equals(bt_ajouter)) {
                System.out.println("bouton ajouter relacher");
            }
        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {
            if (mouseEvent.getSource().equals(bt_modifier)) {
                bt_modifier.setForeground(Color.red);
                bt_modifier.setText("en cour de selection");
            }

            if (mouseEvent.getSource().equals(bt_supprimer)) {
                bt_supprimer.setForeground(Color.red);
                bt_supprimer.setText("en cour de selection");
            }
        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {
            if (mouseEvent.getSource().equals(bt_modifier)) {
                bt_modifier.setForeground(Color.black);
                bt_modifier.setText("modifier");
            }
            if (mouseEvent.getSource().equals(bt_supprimer)) {
                bt_supprimer.setForeground(Color.black);
                bt_supprimer.setText("supprimer");
            }
        }
    }

    public class Alistener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (rb_annee1.isSelected())
            {
                pnl_semA1.setVisible(true);
                pnl_semA2.setVisible(false);
                lbl_sem.setVisible(true);

            }
            if (rb_annee2.isSelected())
            {
                pnl_semA1.setVisible(false);
                pnl_semA2.setVisible(true);
                lbl_sem.setVisible(true);

            }
        }
    }
}
